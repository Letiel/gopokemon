var loadState = {
	preload: function() {
		game.load.image('sky', 'assets/sky.png');
		game.load.image('montanha', 'assets/montanha.png');
		game.load.image('nuvem', 'assets/nuvem.png');
	    game.load.image('ground', 'assets/platform.png');
	    // game.load.image('star', 'assets/star.png');
	    // game.load.image('fire', 'assets/fire.png');

	    game.load.spritesheet('charmander', 'assets/charmander.png', 42, 42);
	    game.load.spritesheet('charmeleon', 'assets/charmileon.png', 70, 66);
	    game.load.spritesheet('charizard', 'assets/charizard.png', 150, 86);
	    game.load.spritesheet('macaco', 'assets/voltorb.png', 38, 37);
	    game.load.spritesheet('pokebola', 'assets/pokebola.png', 25, 26);

	    game.load.image('flag','assets/flag.png');

	    // game.load.audio('macaco','assets/audio/macaco.mp3');
	},

	create: function() {
		game.state.start('menu');
	},
}