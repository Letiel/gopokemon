var game = new Phaser.Game(800, 600, Phaser.AUTO, 'gameDiv');

game.state.add('boot',bootState);
game.state.add('load',loadState);
game.state.add('menu',menuState);
game.state.add('play',playState);
game.state.add('play2',playState2);
game.state.add('play3',playState3);
game.state.add('win',winState);
game.state.add('win2',winState2);
game.state.add('win3',winState3);

game.state.start('boot');