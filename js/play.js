var playState = {
    create: function() {

        //game.world.resize(3000, 600);

        game.time.reset();

        game.world.setBounds(0, 0, 3000, 600);

        //  A simple background for our game
        game.add.sprite(0, 0, 'sky');

        game.add.sprite(2780,482, 'flag');

        //  Finally some stars to collect
        pokebolas = game.add.group();

        //  We will enable physics for any star that is created in this group
        pokebolas.enableBody = true;

        //  Here we'll create 12 of them evenly spaced apart
        for (var i = 0; i < 10; i++)
        {
            //  Create a star inside of the 'stars' group
            var pokebola = pokebolas.create(i * 400, 0, 'pokebola');

            //  Let gravity do its thing
            pokebola.body.gravity.y = (Math.random() * 300) + 100;

            //  This just gives each star a slightly random bounce value
            //pokebola.body.bounce.y = 0.7 + Math.random() * 0.2;
        }

        //  The platforms group contains the ground and the 2 ledges we can jump on
        platforms = game.add.group();

        //  We will enable physics for any object that is created in this group
        platforms.enableBody = true;

        // Here we create the ground.
        var ground = platforms.create(0, game.world.height - 64, 'ground');

        //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
        ground.scale.setTo(2, 2);

        //  This stops it from falling away when you jump on it
        ground.body.immovable = true;

        /*//  Now let's create two ledges
        var ledge = platforms.create(400, 400, 'ground');
        ledge.body.immovable = true;

        ledge = platforms.create(-150, 250, 'ground');
        ledge.body.immovable = true;*/

        // The player and its settings
        player = game.add.sprite(32, game.world.height - 150, 'charmander');

        //  We need to enable physics on the player
        game.physics.arcade.enable(player);

        //  Player physics properties. Give the little guy a slight bounce.
        player.body.bounce.y = 0.1;
        player.body.gravity.y = 500;
        player.body.collideWorldBounds = true;

        professores = game.add.physicsGroup(
            Phaser.Physics.ARCADE,
            game.world,
            'professores'  
        );

        professores.enableBody = true;

        professores.collideWorldBounds = true;
        /*for(i=1;i<=16;i++){

            if(i%2==0)
                inimigo = professores.create(200*i, game.world.height - 110, 'dude2');
            else
                inimigo = professores.create(200*i, game.world.height - 145, 'dude4');

            inimigo.animations.add('walk', [0,1,2,3]);
            inimigo.play('walk', 10, true);
            inimigo.body.velocity.setTo(-20,0);   

            inimigo.body.immovable = true;
        }*/

        macaco = game.add.physicsGroup(
            Phaser.Physics.ARCADE,
            game.world,
            'macaco'  
        );

        macaco.enableBody = true;

        macaco.collideWorldBounds = true;

        goku = macaco.create(-250, game.world.height - 110, 'macaco');

        goku.animations.add('walk-right', [6,5,4,3,2,1,0]);
        goku.animations.add('walk-left', [7,8,9,10,11,12,13]);
        goku.play('walk-right', 7, true);
        goku.body.velocity.setTo(+100,0);   

        goku.body.immovable = true;

        music = game.add.audio('macaco');

        // music.play();

        //---------------------------------------



        //  Our two animations, walking left and right.

        // ANIMAÇÃO DO CHARMANDER
        player.animations.add('right', [5,4,3,2,1,0], 10, true);
        player.animations.add('left', [6, 7, 8, 9,10], 10, true);

        // player.animations.add('right', [0,1,2,3,4,5,6,7], 10, true);
        // player.animations.add('left', [8,9,10,11,12,13,14,15], 10, true);

        // //  The score
        // scoreText = game.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#FFF' });
        // scoreText.fixedToCamera = true;

        //  Our controls.
        cursors = game.input.keyboard.createCursorKeys();
        
    },

    // morte: function(){
    //     if(score > 10)
    //         score -= 10;
    //     else
    //         score = 0;
    //     scoreText.text = 'Score: ' + score;
    // },

    update: function() {

        game.physics.arcade.collide(player, professores, this.playerHitSprite, null, this);

        game.physics.arcade.collide(player, macaco, this.playerHitMacaco, null, this);

        game.physics.arcade.collide(pokebolas, platforms, this.starsHitPlatform, null, this);

        game.physics.arcade.overlap(player, pokebolas, this.collectStar, null, this);

        this.camera.follow(player, Phaser.Camera.FOLLOW_PLATFORMER);

        //  Collide the player and the stars with the platforms
        game.physics.arcade.collide(player, platforms);

        //  Reset the players velocity (movement)
        player.body.velocity.x = 0;

        if (cursors.left.isDown)
        {
            //  Move to the left
            player.body.velocity.x = -150;

            player.animations.play('left');
        }
        else if (cursors.right.isDown)
        {
            game.camera.x += 4;

            //  Move to the right
            player.body.velocity.x = 150;

            player.animations.play('right');
        }
        else
        {
            //  Stand still
            player.animations.stop();

            player.frame = 0;
        }
        
        //  Allow the player to jump if they are touching the ground.
        if (cursors.up.isDown && player.body.touching.down)
        {
            player.body.velocity.y = -350;
        }

        if(player.body.x >= 2800)
        {
            this.win();
        }

        /*if(marcia.body.touching.up && !morto){
            console.log("Matou!");
            morto = true;
        }

        if(marcia.body.touching.left || marcia.body.touching.right){
            morte();
        }*/

        //console.log(goku.body.x);
        if(player.body.x < goku.body.x -10) {
             goku.body.velocity.setTo(-120,0);
             goku.play('walk-left', 7, true);
        } else if(player.body.x > goku.body.x) {
             goku.body.velocity.setTo(+120,0);
             goku.play('walk-right', 7, true);
        } else {
             goku.body.velocity.setTo(0,0);
             goku.frame = 5;
        }

    },

    playerHitSprite: function(player,x){
        if(x.body.touching.up){
            // score+=100;
            // scoreText.text = 'Score: ' + score;
            x.kill();
        }

        if(x.body.touching.left || x.body.touching.right){
            player.body.x = 32;
            // this.morte();
        }
    },

    playerHitMacaco: function(player,x){
        console.log("bateu");
        if(x.body.touching.left || x.body.touching.right || x.body.touching.up){
            player.body.x = 32;
        }
    },

    starsHitPlatform: function(stars,platforms){
        if(stars.body.touching.down){
            stars.body.y = 0;
        }
    },

    collectStar: function(player, star) {
        player.body.x = 32;
    },

    render: function() {

        //game.debug.cameraInfo(game.camera, 32, 32);
        game.debug.text('Tempo: ' + this.game.time.totalElapsedSeconds(), 32, 32);

    },

    win: function() {
        game.state.start('win');
    }
}